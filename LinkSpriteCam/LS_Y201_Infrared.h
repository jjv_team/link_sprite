/*
  LS_Y201_Infrared.h - Command definition for LS_Y201_Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

#ifndef LS_Y201_Infrared_h
#define LS_Y201_Infrared_h


//// LinkSprite JPEG Color Camera Serial UART Interface With Infrared
/// http://www.linksprite.com/upload/file/1291522825.pdf
namespace LS_Y201_Infrared{

//*** reset powercycle
const uint8_t RESET_COMMAND[] = {0x56, 0x00, 0x26, 0x00};
const uint8_t RESET_RESPONSE[] = {0x76, 0x00, 0x26, 0x00};


//*** Power Saving
const uint8_t ENABLE_POWER_SAVING_COMMAND[]  = {0x56, 0x00, 0x3E, 0x03, 0x00, 0x01, 0x01};
const uint8_t DISABLE_POWER_SAVING_COMMAND[] = {0x56, 0x00, 0x3E, 0x03, 0x00, 0x01, 0x00};
const uint8_t CHANGE_POWER_SAVING_RESPONSE[] = {0x76, 0x00, 0x3E, 0x00, 0x00};


//*** Take picture
const uint8_t TAKE_PICTURE_COMMAND[] = {0x56, 0x00, 0x36, 0x01, 0x00};
const uint8_t TAKE_PICTURE_RESPONSE[]= {0x76, 0x00, 0x36, 0x00, 0x00};


//*** Stop taking pictures
const uint8_t TAKE_PICTURE_STOP_COMMAND[] = {0x56, 0x00, 0x36, 0x01, 0x03};
const uint8_t TAKE_PICTURE_STOP_RESPONSE[]= {0x76, 0x00, 0x36, 0x00, 0x00};


/*** Read JPEG file size
XH XL is the file length of JPEG file, MSB is in the front, and followed
by LSB.
*/
const uint8_t ASK_JPEG_SIZE_COMMAND[] = {0x56, 0x00, 0x34, 0x01, 0x00};
const uint8_t ASK_JPEG_SIZE_RESPONSE_HEADER[] = {0x76, 0x00, 0x34, 0x00, 0x04, 0x00, 0x00};// XH XL


//*** Compression Ratio
const uint8_t SET_COMPRESS_RATIO_COMMAND_HEADER[] = {0x56, 0x00, 0x31, 0x05, 0x01, 0x01, 0x12, 0x04}; // XX
const uint8_t SET_COMPRESS_RATIO_RESPONSE[] = {0x76, 0x00, 0x31, 0x00, 0x00};

/*** JPEG file content
JPEG file starts with FF D8 and ends with FF D9.
To read the JPEG file, always starts with address 00 00,and choose a
chunk size that are an integer times of 8, and read the chunk many
times until reds FF D9 which indicates the end of the JPEG file.
*/
const uint8_t JPEG_GET_CONTENT_COMMAND_HEADER[] = {0x56, 0x00, 0x32, 0x0C, 0x00, 0x0A, 0x00, 0x00}; // MH, ML, 0x00, 0x00, KH, KL, XX, XX
const uint8_t JPEG_GET_CONTENT_RESPONSE_HEADER[] = {0x76, 0x00, 0x32, 0x00, 0x00}; // XX XX
const uint8_t JPEG_CONTENT_INI[] = {0xFF, 0xD8};
const uint8_t JPEG_CONTENT_EOF[] = {0xFF, 0xD9};
const uint8_t DEFAULT_INTERVAL_TIME[] = {0x00, 0x0A};


//*** Image size
const uint8_t IMAGE_SET_SIZE_640_480_COMMAND[] = {0x56, 0x00, 0x31, 0x05, 0x04, 0x01, 0x00, 0x19, 0x00};
const uint8_t IMAGE_SET_SIZE_320_240_COMMAND[] = {0x56, 0x00, 0x31, 0x05, 0x04, 0x01, 0x00, 0x19, 0x11};
const uint8_t IMAGE_SET_SIZE_160_120_COMMAND[] = {0x56, 0x00, 0x31, 0x05, 0x04, 0x01, 0x00, 0x19, 0x22};
const uint8_t IMAGE_SET_SIZE_RESPONSE[] = {0x76, 0x00, 0x31, 0x00, 0x00};
enum {
    RES_640_480 = 0x00,
    RES_320_240 = 0x11,
    RES_160_120 = 0x22,
};


//*** Change baud rate
#define _baud_9600 0xAE, 0xC8
#define _baud_19200 0x56, 0xE4
#define _baud_38400 0x2A, 0xF2
#define _baud_57600 0x1C, 0x4C
#define _baud_115200 0x0D, 0xA6
#define _baud_command 0x56, 0x00, 0x24, 0x03, 0x01

struct baud_table{
    long int rate;
    uint8_t command[7];
};
const baud_table baud_rate_table[] = {
    9600,   {_baud_command, _baud_9600},
    19200,  {_baud_command, _baud_19200},
    38400,  {_baud_command, _baud_38400},
    57600,  {_baud_command, _baud_57600},
    115200, {_baud_command, _baud_115200}
};
const uint8_t SET_BAUD_RATE_RESPONSE[] = {0x76, 0x00, 0x24, 0x00, 0x00};

const uint8_t SET_BAUD_RATE_9600_COMMAND[] = {_baud_command, _baud_9600};
const uint8_t SET_BAUD_RATE_38400_COMMAND[] = {_baud_command, _baud_38400};
const uint8_t SET_BAUD_RATE_115200_COMMAND[] = {_baud_command, _baud_115200};


}//NS

#endif
