/*
  LinkSpriteCam.cpp - Wrapper for serial communication
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

#include "LinkSpriteCam.h"


//// API level 0

uint8_t LinkSpriteCam::blockingRead(){
    int readed;
    while( (readed=readed = ss->read()) < 0 );
    return readed;
}


void LinkSpriteCam::blockingConsume(int len)
{
    for (int i=0; i<len; )
        if (ss->read() >= 0) i++;
}



//// API level 1


void LinkSpriteCam::sendCommand(const uint8_t* command, int len)
{
    for (int i=0; i<len; i++)
        ss->write(command[i]);
}


void LinkSpriteCam::getResponse(uint8_t* buff, const int len){
    for (int i=0; i<len; i++){
        buff[i] = ss->read();
    }
}


bool LinkSpriteCam::checkResponse(const uint8_t expected[], const int len)
{
    int readed=0;
    uint8_t response[len];

    // non-blocking read
#if 1
    readed = ss->readBytes(response, len);
#else
    while(ss->available()>0 && readed<len){
        response[readed] = ss->read();
        readed++;
    }
#endif

    return compareMessages(expected, len, response, readed);
}


/// Blocking I/O

void LinkSpriteCam::blockingGetResponse(uint8_t* buff, const int len)
{
    for (int i=0; i<len; i++)
        buff[i] = blockingRead();
}


bool LinkSpriteCam::blockingCheckResponse(const uint8_t expected[], const int len)
{
    uint8_t response[len];
    blockingGetResponse(response, len);
    return compareMessages(expected, response, len);
}


/// Auxiliar functions

bool LinkSpriteCam::compareMessages(const uint8_t* b1, const uint8_t* b2, const int len)
{
    for (int i=0; i<len; i++)
        if (b1[i] != b2[i])
            return false;

    return true;
}

bool LinkSpriteCam::compareMessages(const uint8_t* b1, const int len1, const uint8_t* b2, const int len2)
{
    //check len and equality
    if (len1!=len2) return false;

    return compareMessages(b1, b2, len1);
}

void LinkSpriteCam::consumeData()
{
    while(ss->available()>0){
        ss->read();
    }
}


bool LinkSpriteCam::consumeSomeData(int len)
{
    if(!ss->available()) return false;

    int i=0;
    for (int i=0; i<len; i++)
        ss->read();

    return i==len;
}



//// API level 2

using namespace LINK_SPRITE_ACTIVE_CONFIG;


bool LinkSpriteCam::sendSetBaudRate(long rate)
{
    int len = sizeof(baud_rate_table)/sizeof(struct baud_table);
    int i;
    for (i=0; i<len; i++){
        if (rate == baud_rate_table[i].rate) break;
    }
    if (i>=len) return false;

    sendCommand(ARG_PLUS_LEN(baud_rate_table[i].command));
    return true;
}


void LinkSpriteCam::sendResetCmd()
{
    sendCommand(ARG_PLUS_LEN(RESET_COMMAND));
}


void LinkSpriteCam::sendTakePhotoCmd()
{
    read_addr=0x0000;
    sendCommand(ARG_PLUS_LEN(TAKE_PICTURE_COMMAND));
}


void LinkSpriteCam::sendReadDataCmd(int chunk_size)
{
    // get internal image pointer in a byte fashion way
    // MH is frame_block index
    // ML is byte index insead frame_block
    MH = read_addr >> 8;
    ML = read_addr & 0xFF;

    KH = chunk_size >> 8;
    KL = chunk_size & 0xFF;

    //IH = DEFAULT_INTERVAL_TIME[0];
    //IL = DEFAULT_INTERVAL_TIME[1];

    sendCommand(ARG_PLUS_LEN(JPEG_GET_CONTENT_COMMAND_HEADER));
    ss->write(MH);
    ss->write(ML);
    ss->write((uint8_t)0x00);
    ss->write((uint8_t)0x00);
    ss->write(KH);
    ss->write(KL);
    ss->write(IH);
    ss->write(IL);

    read_addr+=chunk_size;
}


void LinkSpriteCam::sendStopTakePhotoCmd()
{
    sendCommand(ARG_PLUS_LEN(TAKE_PICTURE_STOP_COMMAND));
    read_addr=0x0000;
}


bool LinkSpriteCam::checkJpegEOF(uint8_t* buff, int at){
    if (at<1) return false;
    return (buff[at-1]==JPEG_CONTENT_EOF[0])
            && (buff[at]==JPEG_CONTENT_EOF[1]);
}



//// API level 3

bool LinkSpriteCam::doReset(int delay_time)
{
    consumeData();
    sendCommand(ARG_PLUS_LEN(RESET_COMMAND));
    bool ok=blockingCheckResponse(ARG_PLUS_LEN(RESET_RESPONSE));
    delay(delay_time);
    consumeData();

    return ok;
}


bool LinkSpriteCam::changeSavingMode(bool enable)
{
    if (enable) sendCommand(ARG_PLUS_LEN(ENABLE_POWER_SAVING_COMMAND));
    else sendCommand(ARG_PLUS_LEN(DISABLE_POWER_SAVING_COMMAND));
    return checkResponse(ARG_PLUS_LEN(CHANGE_POWER_SAVING_RESPONSE));
}


bool LinkSpriteCam::doTakePicture()
{
    sendTakePhotoCmd();
    return blockingCheckResponse(ARG_PLUS_LEN(TAKE_PICTURE_RESPONSE));
}


bool LinkSpriteCam::doStopTakePicture()
{
    sendStopTakePhotoCmd();
    return blockingCheckResponse(ARG_PLUS_LEN(TAKE_PICTURE_STOP_RESPONSE));
}


void LinkSpriteCam::getImageSize(uint8_t &major, uint8_t &minor)
{
    sendCommand(ARG_PLUS_LEN(ASK_JPEG_SIZE_COMMAND));
    blockingCheckResponse(ARG_PLUS_LEN(ASK_JPEG_SIZE_RESPONSE_HEADER));

    major = blockingRead();
    minor = blockingRead();
}


int LinkSpriteCam::getImageSize()
{
    uint8_t major, minor;
    int size;

    getImageSize(major, minor);
    size = ((int)major)<<8 | minor;

    return size;
}


bool LinkSpriteCam::setBaudRate(long rate)
{
    if (sendSetBaudRate(rate))
        return blockingCheckResponse(ARG_PLUS_LEN(SET_BAUD_RATE_RESPONSE));
    else
        return false;
}


bool LinkSpriteCam::setCompressRatio(uint8_t ratio)
{
    sendCommand(ARG_PLUS_LEN(SET_COMPRESS_RATIO_COMMAND_HEADER));
    ss->write(ratio);
    bool ok = blockingCheckResponse(ARG_PLUS_LEN(SET_COMPRESS_RATIO_RESPONSE));
    return ok;
}


bool LinkSpriteCam::takePictureOver(Stream *sout, const unsigned short chunck_size)
{
    uint8_t imbuffer[chunck_size];
    int j,J;
    bool ok=1;
    bool EndOfData=0;

    // reset remote pointer address and perform a photo capture
    read_addr = 0;
    sendTakePhotoCmd();
    ok &= blockingCheckResponse(ARG_PLUS_LEN(TAKE_PICTURE_RESPONSE));


    while (!EndOfData){
        // ask for next chunck
        sendReadDataCmd(chunck_size);
        ok &= blockingCheckResponse(ARG_PLUS_LEN(JPEG_GET_CONTENT_RESPONSE_HEADER));

        j=0;
        while(j<chunck_size){
            imbuffer[j++] = blockingRead();

            // check if the picture is over
            if (checkJpegEOF(imbuffer, j-1)){
                EndOfData=1;
                blockingConsume(chunck_size-j);
                break;
            }
        }

        ok &= blockingCheckResponse(ARG_PLUS_LEN(JPEG_GET_CONTENT_RESPONSE_HEADER));


        for(J=j, j=0; j<J; j++)
            sout->write(imbuffer[j]);
    }

    return ok;
}


bool LinkSpriteCam::setPictureResolution(uint8_t mode){
    sendCommand(IMAGE_SET_SIZE_160_120_COMMAND, sizeof(IMAGE_SET_SIZE_160_120_COMMAND)-1);
    ss->write(mode);
    return blockingCheckResponse(ARG_PLUS_LEN(IMAGE_SET_SIZE_RESPONSE));
}


void LinkSpriteCam::setChunckSize(uint16_t chunck_size){
    KH = chunck_size >> 8;
    KL = chunck_size & 0xFF;
}


void LinkSpriteCam::setIntervalTime(uint16_t time){
    IH = time >> 8;
    IL = time & 0xFF;
}



template <class SerialImpl>
bool LinkSpriteCamSerial<SerialImpl>::changeBaudRate(long int bauds){
    long int current = query_current_baud_rate();
    if (current == bauds) return true;

    sendSetBaudRate(bauds);
    delay(200);
    current = query_current_baud_rate();
    return (current == bauds);
}

template <class SerialImpl>
long int LinkSpriteCamSerial<SerialImpl>::begin(){
    long int current = query_current_baud_rate();
    serialPtr->begin(current);
}

template <class SerialImpl>
long int LinkSpriteCamSerial<SerialImpl>::query_current_baud_rate() {
  int len = sizeof(baud_rate_table)/sizeof(baud_table);

  int i;
  for (i=1; i<len; i++){ // i=1 --> avoid 9600 broken baud rate
    long int bauds = baud_rate_table[i].rate;
    serialPtr->begin(bauds);
    consumeData();
    sendSetBaudRate(bauds);
    delay(200);
    bool ok = checkResponse(ARG_PLUS_LEN(SET_BAUD_RATE_RESPONSE));
    if (ok) return bauds;
  }

  return -1;
}


/// TEMPLATE WORKARROUND
template class LinkSpriteCamSerial<HardwareSerial>;
//template class LinkSpriteCamSerial<SoftwareSerial>;
