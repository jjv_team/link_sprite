/*
  LinkSpriteCam.h - Wrapper for serial communication
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

#ifndef LinkSpriteCamCam_h
#define LinkSpriteCamCam_h

#include <Arduino.h>
#include <inttypes.h>
#include <Stream.h>


class LinkSpriteCam{
public:
    // pointer to base communication stream. Hack it your own
    Stream *const ss;

    //// state dependant variables
    // remote pointer to picture data
    int read_addr=0x0000;

    // `addr` enconded as major,minor
    uint8_t MH,ML;

    // picture size (major, minor)
    uint8_t KH,KL;

    // data stream interval time. (delay between each chunk)
    uint8_t IH=0x00,IL=0x00;
  

public:
    LinkSpriteCam(Stream *serial): ss(serial){}

    //// API level 0 (through Stream).
    /// Auxiliar functions
    // Blocking I/O functions
    uint8_t blockingRead();
    void blockingConsume(int len);

    //// API level 1
    /// Provide capability to work with "chunk of bytes" == messages
    // Send a `command` to device
    void sendCommand(const uint8_t command[], const int len);
    

    /// Non-blocking I/O
    // Read `len` from device
    void getResponse(uint8_t* buff, const int len);

    // Read `sizeof(expected)` from device.
    // Then verify if readed bytes match with `expected` bytes
    bool checkResponse(const uint8_t expected[], const int len);

    // Macro to put the command and it sizeof
    #define ARG_PLUS_LEN(arr) arr, sizeof(arr)

    /// Blocking I/O
    void blockingGetResponse(uint8_t* buff, const int len);
    bool blockingCheckResponse(const uint8_t expected[], const int len);

    /// Auxiliar functions
    // Drop all queued bytes from device
    // It should let the input clean
    void consumeData();

    // Drop `len` bytes from device
    // blocking reading until len
    bool consumeSomeData(int len);

    // Check if two messages are equals
    bool compareMessages(const uint8_t* b1, const int len1, const uint8_t* b2, const int len2);
    bool compareMessages(const uint8_t* b1, const uint8_t* b2, const int len);



    //// API level 2
    /// Provide message abstraction
    // Send Reset command
    // It must be called:
    //  * at init
    //  * after change compression
    //  * after change picture size
    void sendResetCmd();

    // Send take picture command
    // It must be called:
    //  * Once before `sendReadDataCmd`
    void sendTakePhotoCmd();

    // Read picture data
    // Order device to send us `chunck_size` bytes.
    void sendReadDataCmd(int chunk_size);

    // Halt picture recovery
    // It must be called:
    //  * on drop picture (no data reading)
    //  * after `sendTakePhotoCmd`
    void sendStopTakePhotoCmd();


    // Verify end of picture data stream
    // It must be called:
    //  * on each chunk fetched with `sendReadDataCmd`
    bool checkJpegEOF(uint8_t* buff, int at);
    
    
    // Change baud rate
    bool sendSetBaudRate(long rate);



    //// API level 3
    /// Provide full send-receive abstraction (bocking I/O)
    bool doReset(int delay_time=2000);
    bool changeSavingMode(bool enable=false);

    bool doTakePicture();
    bool doStopTakePicture();

    // Get image size of current captured frame
    //  * requires `doTakePicture` first
    void getImageSize(uint8_t &major, uint8_t &minor);

    // Return human-readable size (in bytes)
    // by composing `major` `minor`
    int getImageSize();

    // send change baud rate command
    //  * it is LOST after RESET_COMMAND
    bool setBaudRate(long rate);

    bool setCompressRatio(uint8_t ratio);

    // Perform a photo and retrieve it through passed Stream
    // `chunck_size` shoud be a multiple of 8.
    //  recomended size is 32.
    bool takePictureOver(Stream *sout, const unsigned short chunck_size=32);

    // change picture resolution
    //  * requires power cycle reset.
    bool setPictureResolution(uint8_t mode);

    // object modifiers (no device communication)
    void setChunckSize(uint16_t chunck_size);
    void setIntervalTime(uint16_t time);

};


template <class SerialImpl>
class LinkSpriteCamSerial : public LinkSpriteCam{
public:
  SerialImpl *const serialPtr;
public:
  LinkSpriteCamSerial<SerialImpl>(SerialImpl *ps):
    LinkSpriteCam(ps), serialPtr(ps)
  {}


  // Begin serial communication.
  //  return device baudrate
  //  or -1 on failure
  long int begin();

  // unsafe method for change baud rate
  //  * it is LOST after RESET_COMMAND
  // BUT
  //  1. device baud rate is reset
  //  2. port baud rate is maintained
  // So communication is broken here
  //  => call is ALLWAYS after RESET_COMMAND or communication will hangs
  bool changeBaudRate(long int baud_rate);

  // Return current baud rate (slow method)
  long int query_current_baud_rate();
};



// Some commands are generic. Anothers are below definition dependant.
// For these, and perhaps new one, code bind is required. (assuming
// other will have same name).
#ifndef LINK_SPRITE_ACTIVE_CONFIG
#define LINK_SPRITE_ACTIVE_CONFIG LS_Y201_Infrared
#endif

#include "LS_Y201_Infrared.h"


#endif
