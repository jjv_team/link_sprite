# LinkSprite JPEG Color Camera TTL Interface - Infrared #
## Serial API for Camera communication ##
*By now only LS_Y201_Infrared ins implemented.*

* [Official buy store](https://store.linksprite.com/infrared-jpeg-color-camera-serial-uart-ttl-level/)
* [Updated version buy store](https://www.sparkfun.com/products/11610)

* [Manual v1.0](http://www.linksprite.com/upload/file/1291522825.pdf)
* [Manual v1.1](http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/LightImaging/UserManual.pdf)


### Others libraries
Most of them had been discover lately. So take a look.

* [ARM mbed Library](https://developer.mbed.org/components/Camera-LS-Y201/) (see also [this](http://developer.mbed.org/users/pm21gt/notebook/linksprite-jpeg-color-camera-ttl-infrared/))
* [JPEGCamera](https://code.google.com/p/linksprite-serial-cam-arduino/)
* [2013 Jenn Case port](http://robotic-controls.com/learn/arduino/linksprite-jpeg-camera)


## Install
Copy **LinkSpriteCam** directory into *Arduino library tree*.
It coud be:

* ~/Arduino/libraries
* ~/$ARDUINO_IDE/libraries
* or whatever valid directory

## Use
Include the library and then create an instance.

It works with Serial and SoftwareSerial
```
#!C++

#include <"LinkSpriteCam.h>

LinskSpriteCam<HardwareSerial> camera(&Serial1); // HardwareSerial #1

SoftwareSerial mySerial(4,5)
LinskSpriteCam<SoftwareSerial> camera(&mySerial); // SoftwareSerial pin-out
```


## Project Info
This project is realized as part of a project under the *Master Visión Artifical, URJC* umbrella.

* State: beta
* Version: 0.9


### Contribute ###
Project is under LGPL 2.1 License.

You are free to fork and improve this code.
I will be happy to merge pull requests.

Only two requirements to contribute:

1. Do clean code (better than me is easy ;))
2. Add your name and modification date at end of license header
