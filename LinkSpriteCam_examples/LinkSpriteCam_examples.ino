/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
  
  Main repository at:
  https://bitbucket.org/jjv_team/link_sprite
  
  Created using code and documentation from
  http://linksprite.com/wiki/index.php5?title=JPEG_Color_Camera_Serial_UART_Interface_(TTL_level)
  
  Special thanks to Jenn Case and to rdpeters93, which code should had been the reference, and I
  compared a-posteriori for code integrity
  http://robotic-controls.com/learn/arduino/linksprite-jpeg-camera
  http://forum.linksprite.com/index.php?/topic/4039-missing-header-on-infrared-linksprite-jpeg-camera/

*/


// Examples' common code
// following Arduino's programming style:
//   * all global variables should be in the principal ino file
// Notice that:
//   * All examples uses "camera" variable as device serial port interface
//   * Default serial device is SERIAL3. Only works with Arduino Mega and similars.
//   * Info and Debug is written through Serial (hardcored)


#include <Arduino.h>
#include <LinkSpriteCam.h>


//#define USE_SOFTWARE_SERIAL
#ifdef USE_SOFTWARE_SERIAL
#include <SoftwareSerial.h>

SoftwareSerial sofwareSerial(4,5); //SoftwareSerial(RX,TX) --> connect (TX,RX)
LinkSpriteCamSerial<SoftwareSerial> camera(&sofwareSerial);

#else
#include <HardwareSerial.h>

LinkSpriteCamSerial<HardwareSerial> camera(&Serial3);
#endif


#include "minimal_examples.h"
//#include "take_picture.h"
//#include "take_picture2.h"
//#include "resurrect_camera.h"



void setup()
{ 
    Serial.begin(19200);
    camera.begin();
    Serial.println("Init done.");
    
    
    //// resurrect camera
    //resurrect_nonblocking();
    //resurrect_blocking();


    camera.doReset();
    Serial.println("Setup done.");
}

void loop() 
{

    //// tests that check responses
    //test_minimal_check_reset_cmd();
    //test_minimal_check_jpeg_size_cmd
    //test_minimal_check_set_compress_ratio();


    //// camera usage minimal examples
    //example_minimal_send_reset_nonblocking();
    //example_minimal_send_reset_blocking();
    //example_minimal_get_jpeg_size_blocking();
    //example_minimal_capture_two_images();
    example_minimal_change_baudrate();


    //// camera usage examples
    //example_show_jpeg_size();
    //example_take_picture_ascii();
    //example_take_picture2_ascii();
    
    
    delay(2000);
}
