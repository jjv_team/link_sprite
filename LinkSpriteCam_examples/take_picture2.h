/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/


// Take a picture and send it as ASCII by Serial port comminication
// Blocking implementation using level 3 API


#include <Arduino.h>
#include <Stream.h>
#include "utils.h"


// Create custom Stream to intercept binary data and convert to ascii
class HexStream : public Stream{
  int i=0;
  int chunk=32;
public:
  size_t write(uint8_t b){
    print_pretty_hex(b);
    if (++i %chunk ==0){
      Serial.println();
      i=0;
    }
    return 1;
  }
  void flush(){
    i=0;
  }
  int available(){return 0;}
  int read(){return -1;}
  int peek(){return -1;}
};


void example_take_picture2_ascii(){
  HexStream hexstream;
  camera.takePictureOver(&hexstream);
  hexstream.flush();
  Serial.println();
  Serial.println();
  
  // camera do video stream with jpeg-diff encode
  // doStopTakePicture will force to send fully each jpeg
  camera.doStopTakePicture();
}

