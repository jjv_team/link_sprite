/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/


// Take a picture and send it as ASCII by Serial port comminication
// Blocking implementation using up to level 2 API


#include <Arduino.h>
#include "utils.h"

void example_take_picture_ascii(){
    const int BUFFER_SIZE = 32;
    byte imbuffer[BUFFER_SIZE];
    int j,J,count;
    bool ok;

    Serial.print("SendTakePhotoCmd... ");
    camera.read_addr = 0;
    camera.sendTakePhotoCmd();
    ok = camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::TAKE_PICTURE_RESPONSE));
    Serial.println(ok);

    //get image size
    int image_size = camera.getImageSize();
    Serial.print("  image size="); Serial.println(image_size);

    J=0;
    bool EndFlag=0;
    while(!EndFlag){
        j=0;
        count=0;

        /// All replies start and end with:
        /// 76 00 32 00 00
        /// Also jpeg start with FF D8 and ends with FF D9
        //@@Serial.print("SendReadDataCmd... ");
        camera.sendReadDataCmd(BUFFER_SIZE);
        ok = camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::JPEG_GET_CONTENT_RESPONSE_HEADER));
        //@@Serial.print(ok);

        while(j<BUFFER_SIZE){
            imbuffer[j++] = camera.blockingRead();

            //Check if the picture is over
            if (camera.checkJpegEOF(imbuffer, j-1)){
                EndFlag=1;
                camera.blockingConsume(BUFFER_SIZE-j);
                break;
            }
        }
        J+=j;


        ok = camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::JPEG_GET_CONTENT_RESPONSE_HEADER));
        //@@Serial.println(ok);


        int count = j;
        if (count>0){
            //Send jpeg picture over the serial port
            for(j=0;j<count;j++){
                print_pretty_hex(imbuffer[j]);
            }
            Serial.println();
        }

        if (EndFlag){ Serial.print("EOF!   readed chunck="); Serial.print(j); Serial.print("   size="); Serial.println(J); }
    }
    Serial.print("Image taken!   size="); Serial.println(J);
    delay(500);



#if 0 // run once
   //shutdown communication
   camera.serialPtr->end();
   Serial.println("Shutdown communication");
   delay(100);

   // loop run once
   while(1);
#endif
}

