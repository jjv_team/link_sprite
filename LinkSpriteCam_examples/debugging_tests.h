/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

#include <Arduino.h>


#include "utils.h"


void test_minimal_check_reset_cmd(){
    Serial.print("RESET_COMMAND ");
    camera.sendResetCmd();
    delay(25);
    read_and_display(sizeof(LS_Y201_Infrared::RESET_RESPONSE));
}


void test_minimal_check_jpeg_size_cmd(){
    Serial.print("ASK_JPEG_SIZE_COMMAND ");
    /// 76 00 34 00 04 00 00 XH XL
    camera.consumeData();
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::ASK_JPEG_SIZE_COMMAND));
    read_and_display(9);
}

// this test check specification v1.0 typo: compress ratio response include ratio on response XX
// it was fixed on specification v1.1
void test_minimal_check_set_compress_ratio(){
    Serial.print("SET_COMPRESS_RATIO_COMMAND... ");
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::SET_COMPRESS_RATIO_COMMAND_HEADER));
    camera.serialPtr->write(0x36);
    //Serial.println(camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::SET_COMPRESS_RATIO_RESPONSE)));
    read_and_display(10);
}
