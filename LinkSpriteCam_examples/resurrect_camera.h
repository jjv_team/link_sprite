/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

// Command sequence to recover device to a normal state.
/* Sometimes, full device dettach is also needed.
   Symptomatology: non-blocking use, for example ask jpeg size, 
   obtains allways a zero from Stream.available(). So no byte
   is readed and sent.
   
   How to detect: if resurrect_blocking, then the problem is
   truly pressented.
   
   Solve:
   a) resurrect_blocking
   b) resurrect_nonblocking
   c) dettach, wait and attach + resurrect_nonblocking
*/
 

#include <Arduino.h>

void resurrect_blocking(){
    Serial.print("doing RESET...");
    Serial.println(camera.doReset());

    Serial.print("DISABLE_POWER_SAVING_COMMAND... ");
    Serial.println(camera.changeSavingMode(false));
}


void resurrect_nonblocking(){
    camera.consumeData();
    Serial.print("doing RESET...");
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::RESET_COMMAND));
    delay(1000);
    Serial.println(camera.checkResponse(ARG_PLUS_LEN(LS_Y201_Infrared::RESET_RESPONSE)));
    delay(3000);
    camera.consumeData();

    Serial.print("DISABLE_POWER_SAVING_COMMAND... ");
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::DISABLE_POWER_SAVING_COMMAND));
    delay(25);
    Serial.println(camera.checkResponse(ARG_PLUS_LEN(LS_Y201_Infrared::CHANGE_POWER_SAVING_RESPONSE)));
}
