/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

#ifndef UTILS_H
#define UTILS_H


void print_pretty_hex(int b){
    if (b<0) Serial.print(b);
    else{
        if(b<0x10) Serial.print("0"); //padding
        Serial.print(b,HEX);
    }
    Serial.print(" ");
}


void read_and_display(int len){
    for (int i=0; i<len; i++)
        print_pretty_hex(camera.serialPtr->read());
    Serial.println();
}


#endif // UTILS_H
