/*
  LinkSprite Demo - Examples for use LS Y201 Infrared
  Copyright (c) 2015 Victor Arribas <v.arribas.urjc@gmail.com>
  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, get a coply from
  <https://www.gnu.org/licenses/lgpl-2.1.html>
*/

#include <Arduino.h>


#include "utils.h"


void example_minimal_send_reset_nonblocking(){
    Serial.print("RESET... ");
    camera.consumeData();
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::RESET_COMMAND));
    delay(3000);
    Serial.println(camera.checkResponse(ARG_PLUS_LEN(LS_Y201_Infrared::RESET_RESPONSE)));
    delay(2000);
    camera.consumeData();
}

void example_minimal_send_reset_blocking(){
    Serial.print("RESET (blocking I/O)... ");
    camera.consumeData();
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::RESET_COMMAND));
    Serial.println(camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::RESET_RESPONSE)));
    delay(2000);
    camera.consumeData();
}


void example_minimal_get_jpeg_size_blocking(){
    /// 76 00 34 00 04 00 00 XH XL
    Serial.print("ASK_JPEG_SIZE (blocking I/O)... ");
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::ASK_JPEG_SIZE_COMMAND));
    Serial.print(camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::ASK_JPEG_SIZE_RESPONSE_HEADER)));

    int major = camera.blockingRead();
    int minor = camera.blockingRead();
    int imsize = major<<8 | minor;

    Serial.print(": "); print_pretty_hex(major); print_pretty_hex(minor);
    Serial.print(" size in bytes:"); Serial.print(imsize);
    Serial.println();
}


void example_show_jpeg_size(){
    Serial.print("TAKE_PICTURE_COMMAND (blocking I/O)... ");
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::TAKE_PICTURE_COMMAND));
    Serial.println(camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::TAKE_PICTURE_RESPONSE)));

    example_minimal_get_jpeg_size_blocking();
    example_minimal_get_jpeg_size_blocking();

    Serial.print("TAKE_PICTURE_STOP_COMMAND (blocking I/O)... ");
    camera.sendCommand(ARG_PLUS_LEN(LS_Y201_Infrared::TAKE_PICTURE_STOP_COMMAND));
    Serial.println(camera.blockingCheckResponse(ARG_PLUS_LEN(LS_Y201_Infrared::TAKE_PICTURE_STOP_RESPONSE)));
}


void example_minimal_capture_two_images(){
    Serial.println("Take 2 photos and say is size:");
    camera.doTakePicture();
    int size1 = camera.getImageSize();
    camera.doStopTakePicture();
    Serial.print("  picture1 size: "); Serial.println(size1);

    camera.doTakePicture();
    int size2 = camera.getImageSize();
    int idemp = camera.getImageSize();
    camera.doStopTakePicture();
    Serial.print("  picture2 size: "); Serial.print(size2); Serial.print(" idempotence: "); Serial.println(idemp);
    Serial.println();
}



void example_minimal_change_baudrate(){
    Serial.print("Current baud rate: ");
    Serial.println(camera.query_current_baud_rate());
    
    long int bauds = 115200;
    
    Serial.print("Setting baud rate to "); Serial.println(bauds);
    Serial.print("success? "); Serial.println(camera.changeBaudRate(bauds));
}
